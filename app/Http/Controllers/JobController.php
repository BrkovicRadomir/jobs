<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobRequest;
use App\Job;
use App\Mail\FirstJobEmail;
use App\Mail\WelcomeEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class JobController extends Controller
{
    protected  $jobs;


  public function __construct(Job $job){
      $this->jobs = $job;
  }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){

       $jobs = $this->jobs->where(['public' => true, 'spam' => false])->orderBy('title', 'ASC')->get();

        return view('jobs.index', compact('jobs'));
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($slug){
        $job = $this->jobs->findBySlug($slug);

        if(!$job)
            return redirect()->route('jobs.index');

        return view('jobs.show', compact('job'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){
        return view('jobs.create');
    }


    /**
     * @param JobRequest $request
     * @return mixed
     */
    public function store(JobRequest $request){

        // get all job offers of this author
        $oldJobs = $this->jobs->where('email', $request->get('email'))->get();

        $input = $request->all();
        // is first job offer
        if(!$oldJobs->count()){
            $input['public'] = false;
            $newJob = $this->jobs->create($input);
            // send email to author
            Mail::to($newJob->email)->send(new WelcomeEmail($newJob));
            // send email to moderator
            Mail::to(env('MODERATOR_EMAIL'))->send(new FirstJobEmail($newJob));
            return redirect()->route('jobs.index')->withFlashMessage("You are successfully create job offer, it will be visible when our moderator allow it. ")->withFlashType('success');
        } else {
            // get all spam offer from author
            $spamsJobs = $oldJobs->where('spam', true)->all();

            // author has spam offer
            if(count($spamsJobs)){
                return redirect()->route('jobs.index')->withFlashMessage("You can't create job offers because moderator marked you as spam.")->withFlashType('danger');

            }
            // author hasn't any spam offer
            else {
                $this->jobs->create($input);
                return redirect()->route('jobs.index')->withFlashMessage("You are successfully create job offer.")->withFlashType('success');
            }
        }

    }


    /**
     * @param $slug
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus($slug, $status){
        $job = $this->jobs->findBySlug($slug);

        if($job){
            switch($status){
                case "public":
                    $job->public = true;
                    $job->save();

                break;
                case "spam":
                    $job->spam = true;
                    $job->save();
                break;

            }

        }

        return redirect()->route('jobs.index');

    }






}
