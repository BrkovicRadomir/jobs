<?php

namespace App\Mail;

use App\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FirstJobEmail extends Mailable
{
    use Queueable, SerializesModels;

  protected  $job;

    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $job = $this->job;
        return $this->subject("New job Offer ".$job->title)->view('emails.first_job', compact('job'));
    }
}
