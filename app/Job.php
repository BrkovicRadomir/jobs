<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    // set Sluggable trait
    use Sluggable;


    protected $table = 'jobs';

    protected $guarded = ['id'];


    /**
     * Make unique slug from job title
     * @return array
     */
    public function sluggable(){
        return [
            'slug' => [
                'source' => "title"
            ]
        ];
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug){
        return $this->where('slug', $slug)->first();
    }

}
