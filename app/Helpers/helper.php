<?php

function resizeText($text, $limit = 160)
{

    // Check if length is larger than the character limit
    if (strlen(strip_tags($text)) > $limit) {  // If so, cut the string at the character limit
        $output = substr(strip_tags($text), 0, $limit);
        // Trim off white space
        $output = trim($output);
        // Add at end of text ...
        return $output . "...";
    } // If not just return the text as is
    else {
        return strip_tags($text);
    }

}
