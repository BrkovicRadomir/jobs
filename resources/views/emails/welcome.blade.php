<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>

<p>
    Hi,<br>
    You have created your first job offer on Test App. You can see your offer on link below <br>
    <a href="{{ route('jobs.show', $job->slug) }}">{{ route('jobs.show', $job->slug) }}</a>

</p>
</body>
</html>