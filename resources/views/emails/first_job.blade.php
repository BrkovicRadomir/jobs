<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
</head>
<body>

<p>
    Hi,<br>
    You have new job offer Test App. You can see your offer below <br>
    <strong>Author:</strong> {{ $job->email }}<br />
    <strong>Title:</strong> {{ $job->title }}<br />
    <strong>Description:</strong> {!! nl2br($job->description) !!} <br />

    <br />
    <br />

    Actions <br>

    <a href="{{ route('jobs.change_status', [$job->slug, 'public']) }}" class="pull-left btn btn-lg btn-success" style="margin-right: 75px;">Approve</a>

    <a href="{{ route('jobs.change_status', [$job->slug, 'spam']) }}" class="pull-right btn btn-lg btn-danger">Make as spam</a>



</p>
</body>
</html>