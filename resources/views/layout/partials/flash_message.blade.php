@if(Session::has('flash_message'))
    <div class="col-md-12">
        <div class="alert alert-{{ Session::has('flash_type') ? Session::get('flash_type') : 'warning' }} alert-dismissible" role="alert">
            {!! Session::get('flash_message') !!}</div>
    </div>
@endif