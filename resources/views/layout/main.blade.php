<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ isset($title) ? $title : "Jobs test" }}</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body>

    <section>
        <div class="container">
            <div class="raw">
                @include('layout.partials.flash_message')
                @yield('content')
            </div>
        </div>

    </section>


<script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>