@extends('layout.main', ['title' => $job->title])


@section('content')
    <div class="col-lg-2 col-md-2  col-sm-4 ">
        <a href="{{ route('jobs.index') }}" class="btn-default btn" >Back</a>
    </div>
    <h1>{{ $job->title }}</h1>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>{!! nl2br($job->description) !!} </p>
                    </div>



@endsection