@extends('layout.main', ['title' => "Add offer"])


@section('content')
    <h1>Add offer</h1>

    {!! Form::open(['route' => ['jobs.store']]) !!}
    <div class="form-group {{ $errors->has('title') ? "has-error" : null }} ">
        <label>Title *</label>
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors ->first('title', '<p class="error-msg">:message</p>') !!}

    </div>

    <div class="form-group {{ $errors->has('email') ? "has-error" : null }} ">
        <label>Email *</label>
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        {!! $errors ->first('email', '<p class="error-msg">:message</p>') !!}

    </div>


    <div class="form-group {{ $errors->has('description') ? "has-error" : null }} ">
        <label>Description *</label>
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors ->first('description', '<p class="error-msg">:message</p>') !!}

    </div>

    <div class="form-group">
        <button class="btn btn-lg btn-success">Submit</button>
        <a href="{{ route('jobs.index') }}" class="btn btn-lg btn-danger pull-right">Cancel</a>
    </div>




    {!! Form::close() !!}

@endsection