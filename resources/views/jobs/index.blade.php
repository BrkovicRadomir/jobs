@extends('layout.main', ['title' => "Job offers"])


@section('content')
    <h1>Job offers</h1>
    <div class="col-lg-2 col-lg-offset-10 col-md-2 col-md-offset-10 col-sm-4 col-sm-offset-8 col-xs-6 col-xs-offset-3 ">
        <a href="{{ route('jobs.create') }}" class="btn-success btn btn-lg" >Add offer</a>
    </div>

    @if($jobs->count())

        @foreach($jobs as $job)
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>{{ $job->title }}</h2>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>{{ resizeText($job->description) }}</p>
                    </div>

                    <div class="col-lg-2 col-lg-offset-10 col-md-2 col-md-offset-10 col-sm-4 col-sm-offset-8 col-xs-6 col-xs-offset-3 ">
                        <a href="{{ route('jobs.show', $job->slug) }}" class="btn-default btn btn-md" >View more</a>
                    </div>
                </div>

            </div>

        @endforeach
    @else

        <p>There is no job offers</p>
    @endif

@endsection