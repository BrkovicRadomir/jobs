<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['uses' => 'JobController@index']);

Route::group(['prefix'=> 'jobs' ], function(){

    Route::get('/', ['as' => 'jobs.index', 'uses' => 'JobController@index']);
    Route::get('/create', ['as' => 'jobs.create', 'uses' => 'JobController@create']);
    Route::post('/store', ['as' => 'jobs.store', 'uses' => 'JobController@store']);
    Route::get('/{slug}', ['as' => 'jobs.show', 'uses' => 'JobController@show']);

    Route::get('/{slug}/{status}', ['as' => 'jobs.change_status', 'uses' => 'JobController@changeStatus']);
});

