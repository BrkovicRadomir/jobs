# README #


### Synopsis ###

I built  test application in Laravel 5.4 framework on backend side and Bootstrap 3 on front. 

### Installation ###

When you download project from repository. There are a few things what you should to do.

If you are on Linux OS please set permissions with help of next commands

```
#!shell

 cd jobs/
 sudo chmod -R 775 storage/ bootstrap/cache
 sudo chgrp -R www-data storage/ bootstrap/cache
```

Then you have to make **.env** file with help of **.env.example** in *jobs* directory. When you set credentials for database connection and emails in .env you can call next commands in console:


```
#!shell
composer install

php artisan key:generate
php artisan migrate

```

Then in your browser you can see on  [http://localhost/jobs/public/index.php](http://localhost/jobs/public/index.php) my test application and enjoy.